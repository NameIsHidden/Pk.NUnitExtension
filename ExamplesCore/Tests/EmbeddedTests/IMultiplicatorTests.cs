﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************

using NUnit.Framework;
using Pk.NUnitExtension.Attributes;
using Examples.Logic;
using Pk.NUnitExtension.Helpers;

namespace Examples.Tests.EmbeddedTests
{
    [EmbeddedTestFixture]
    public class IMultiplicatorTests: ObjectFactoryHolder<IFactory<IMultiplicator>>
    {
        [TestCase(0, 0, 0, TestName = "a=b=0")] //TestMethod name substituted by TestName.
        [DecoratedTestCase(0, 1, 0, TestName = "a=0, b>0")] //Full test name is combination of TestMethodName(TestName)
        [TestCase(2, 6, 12)]
        [DecoratedTestCase(-2, -6, 12)] //Without sprcified test name appearance shold be the same as usual TestCase
        [TestCase(-2, 6, -12)]
        public void Multiplication(int val1, int val2, int expResult)
        {
            var tst = this.Factory.Create();

            int res = tst.Multiply(val1, val2);

            Assert.AreEqual(expResult, res);
        }
    }
}
