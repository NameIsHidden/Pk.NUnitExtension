﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************

using System;
using NUnit.Framework;
using Pk.NUnitExtension.Attributes;
using Examples.Logic;
using Pk.NUnitExtension.Helpers;
using Examples.Tests.Attributes;

namespace Examples.Tests.EmbeddedTests
{
    [EmbeddedTestFixture()]
    public class IDividerTests
    {
        //So inheritance can be used JIC
        ObjectFactoryHolder<IFactory<IDivider>> _Factory;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            this._Factory = new ObjectFactoryHolder<IFactory<IDivider>>();
        }

        [TestCase(2, 2, 1)]
        [TestCase(0, 1, 0)]
        public void Divide(int val1, int val2, int expected)
        {
            var tst = this._Factory.Factory.Create();

            int res = tst.Divide(val1, val2);

            Assert.AreEqual(expected, res);
        }

        [TestCase(2, 2, 0)]
        [TestCase(0, 1, 0)]
        [TestCase(6, 5, 1)]
        public void Mod(int val1, int val2, int expected)
        {
            var tst = this._Factory.Factory.Create();

            int res = tst.Mod(val1, val2);
            
            Assert.AreEqual(expected, res);
        }

        [DivisionOpTestCase(100, 0)]
        [DivisionOpTestCase(0, 0, TestName = "Divide 0 by 0")]
        public void DivisionInvalidOperation(DivisionTests divider,int val1, int val2)
        {
            var tst = this._Factory.Factory.Create();

            Assert.Throws<DivideByZeroException>(() => { int res = divider(tst, val1, val2); });
        }
    }
}