﻿// // ***********************************************************************
// // Copyright (c) 2019 Pavlo Kruglov
// //
// // Permission is hereby granted, free of charge, to any person obtaining
// // a copy of this software and associated documentation files (the
// // "Software"), to deal in the Software without restriction, including
// // without limitation the rights to use, copy, modify, merge, publish,
// // distribute, sublicense, and/or sell copies of the Software.
// //
// // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// // EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// // NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// // LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// // OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// // WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// // ***********************************************************************

using System;
using NUnit.Framework;
using Pk.NUnitExtension.Attributes;
using Examples.Logic;
using Pk.NUnitExtension.Helpers;

namespace Examples.Tests.EmbeddedTests
{
    [EmbeddedTestFixture]
    public class IMathConstantsTests: ObjectFactoryHolder<IFactory<IMathConstants>>
    {
        [Test()]
        public void Zero()
        {
            var tst = Factory.Create();

            int res = tst.Zero;

            Assert.AreEqual(0, res);
        }
    }
}
