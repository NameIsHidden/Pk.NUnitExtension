Package helps to reuse test code and test cases for the classes, that massively implement common intefaces and\or methods that behave equaly with equal input.

Compatible with NUnit 3.11.0 and above. Tested with NUnit 3.11.0 and 3.12.0. Tested with Test Explorers build-in to vs4mac 2019.

Example of usage can be found in master branch in ExamplesCore and ExamplesNet projects. Two separate projects are provided, because tests looks differenly for each platform in Test Explorer in vs4mac.

Nuget package name: Pk.NUnitExtension

# Features #

[Embedded test fixtures](#embedded-test-fixtures)

[Composite test cases](#composite-test-cases)

[Decorative Attributes](#decorative-attributes)

## Embedded test fixtures ##

Assume that you are developing framework\library, that have big number of classes implementing  common interfaces. And it's supposed that if classes instantiated in a similar way with same input, then implemented interfaces behave the same, i.e. it's possible to write common test cases for different classes that implements same interface. How to write testcases and reduce number of copy-paste? In most cases, Abstract Fixture pattern helps to test 'base' functionality and derived fixture contains class' implementation-specific tests. 

But what if some of these common interfaces are independent from each other and classes have to implement them? .net doesn't allow multiple inheritance (like C++ does), so you can't use multiple abstract fixtures.

Real-life example is (writing unit test for) collections:
* LinkedList<> implements ICollection<>, IEnumerable<>, IReadOnlyCollection<T>, ICollection, IEnumerable, IDeserializationCallback ISerializable
* List<> implements ICollection<>, IEnumerable<>, IList<>, IReadOnlyCollection<>, IReadOnlyList<>, ICollection, IEnumerable, IList
* Stack<> implements IReadOnlyCollection<>, IEnumerable<>, IEnumerable.
* Etc.

(Interfaces were copy-pasted from Microsoft docs, so maybe list is inaccurate, but it doesn't make a big difference)

If List or LinkedLinked has same set of elements (1, 4, 6, 3, 8), it's easy to identify equal test cases for common interfaces (CopyTo, Contains, etc. would behave the same}. Of course, there are number implementation-specific edge cases, but there is number of testcases that are identical. Since set of interfaces is very much chaotic, using abstract fixture in a common sense is a tricky (or sometimes impossible) approach. In some cases it's possible to leverage type arguments ([and abstract fixtures](https://github.com/nunit/docs/wiki/TestFixture-Attribute#inheritance)), but in some circumstances it cause mess in source code.

Below you can find how to organize tests using Pk.NUnitExtesion framework with CompositeTestFixture and EmbeddedTestFixture attributes. 
For the sake of example, let's forget about collections and switch to dimplier example. Assume that unit test has to be written for code below:

```csharp
public interface IMultiplicator
{
    int Multiply(int val1, int val2);
}

public interface IMathConstants
{
    int Zero { get; }
}

public class Multiplicator: IMultiplicator
{
    public int Multiply(int val1, int val2) => val1 * val2;
}

public class MathConstants: IMathConstants
{
    public int Zero => 0;
}

public class Calculator: IMultiplicator, IMathConstants
{
    //IMathConstantsTests methods
    public int Zero => 0;

    //IMiltiplicator methods
    public int Multiply(int val1, int val2) => val1 * val2;
}
```

Pk.NUnitExtension framework allows to write separate test fixtures for each of IMultiplicator, IMathConstants, ICalculator interfaces, then include them to “Main” test fixture.

First, define factory interface. Factory will be used to instantiate test objects for appropriate type. Any type can be used as factory. But common generic interface allows to leverage covariance, so only one compatible interface should be implemented in fixtures

```csharp
//Generic factory makes possible using covariance
public interface IFactory<out T>
{
    T Create();
}
```

Define "embedded test fixtures" for IMathConstants and ICalculator. 
```csharp
[EmbeddedTestFixture]
public class IMultiplicatorTests: ObjectFactoryHolder<IFactory<IMultiplicator>>
{
    [TestCase(0, 0, 0)] 
    [TestCase(0, 1, 0)] 
    [TestCase(2, 6, 12)]
    [TestCase(-2, -6, 12)] 
    [TestCase(-2, 6, -12)]
    public void Multiplication(int val1, int val2, int expResult)
    {
        var tst = this.Factory.Create();

        int res = tst.Multiply(val1, val2);

        Assert.AreEqual(expResult, res);
    }
}

[EmbeddedTestFixture]
public class IMathConstantsTests: ObjectFactoryHolder<IFactory<IMathConstants>>
{
    [Test()]
    public void Zero()
    {
        var tst = Factory.Create();

        int res = tst.Zero;

        Assert.AreEqual(0, res);
    }
}
```

Write "Composite test fixtures" for Multiplicator, MathConstants, Calculator, that implement appropriate factory interface.

```csharp
[CompositeTestFixture(typeof(IMultiplicatorTests))]
public class MultiplicatorTests : IFactory<Multiplicator>
{
    public Multiplicator Create() => new Multiplicator();

    //Some implementation specific tests
    [Test()]
    public void MultiplyMultiplicatorSpecificTests()
    {
        var tst = new Calculator();

        int res = tst.Multiply(1, 4);

        Assert.AreEqual(4, res);
    }
}

[CompositeTestFixture(typeof(IMathConstantsTests))]
public class MathConstantsTests:IFactory<MathConstants>
{
    public MathConstants Create() => new MathConstants();

    //Some implementation specific tests
}

[CompositeTestFixture(typeof(IMultiplicatorTests),
                      typeof(IMathConstants))]
public class CalculatorTests: IFactory<Calculator>
{
    //Implement covariantly compatible factory interface for all embedded fixtures
    public Calculator Create() => new Calculator();

    //Some implementation specific tests
    [Test()]
    public void MultiplyCalculatorSpecificTests()
    {
        var tst = new Calculator();

        int res = tst.Multiply(1, 3);

        Assert.AreEqual(3, res);
    }
}
```

As result, separate test cases from IMultiplicatorTests, IMathConstantTests and CalculatorTests are added for Calculator object; from IMultiplicatorTests and MultiplicatorTests added for Multiplicator object; from IMathConstantTests and MathConstantTests added for MathConstant objects. Tests are visible as part of "Composite fixture" in non-defected test explorer (like vs4mac has). 

EmbeddedTestFixture attribute also allows to include another EmbeddedTestFixtures. Let's add extra classes and interfaces to production code.

```csharp
public interface IDivider
{
    int Divide(int val1, int val2);
    int Mod(int val1, int val2);
}

public interface ICalculator : IMultiplicator, IMathConstants
{
}

public interface IComplexCalculator : IDivider, ICalculator
{
    int Min(int a, int b);
}

public class Divider: IDivider
{
    public int Divide(int val1, int val2) => val1 / val2;

    public int Mod(int val1, int val2) => val1 % val2;
}

public class ComplexCalculator: IComplexCalculator
{
    //IComplexDivider methods
    public int Divide(int val1, int val2) => val1 / val2;
    public int Mod(int val1, int val2) => val1 % val2;

    //IMiltiplicator methods
    public int Multiply(int val1, int val2) => val1 * val2;

    //IComplexCalculator methods
    public int Min(int a, int b) => Math.Min(a, b);

    //IMathConstants methods
    public int Zero => 0;
}
```
Then make following changes to unit test code.
```csharp
[EmbeddedTestFixture(typeof(IMultiplicatorTests),
                    typeof(IMathConstants))]
public class ICalculatorTests: ObjectFactoryHolder<IFactory<ICalculator>>
{
    [Test()]
    public void SomeSpecificICalculatorTest()
    {
    }
}

[EmbeddedTestFixture()]
public class IDividerTests
{
    //So inheritance can be used JIC
    ObjectFactoryHolder<IFactory<IDivider>> _Factory;

    [OneTimeSetUp]
    public void OneTimeSetup()
    {
        this._Factory = new ObjectFactoryHolder<IFactory<IDivider>>();
    }

    [TestCase(2, 2, 1)]
    [TestCase(0, 1, 0)]
    public void Divide(int val1, int val2, int expected)
    {
        var tst = this._Factory.Factory.Create();

        int res = tst.Divide(val1, val2);

        Assert.AreEqual(expected, res);
    }

    //Other IDivider tests
}

[CompositeTestFixture(typeof(ICalculatorTests), 
                        typeof(IDividerTests))]
public class ComplexCalculatorTests: IFactory<ComplexCalculator>
{
    //Implement covariantly compatible factory interface for all embedded fixtures
    public ComplexCalculator Create() => new ComplexCalculator();

    //Some implementation specific tests
    [Test()]
    public void MultiplyComplexCalculatorSpecificTests()
    {
        var tst = new Calculator();

        int res = tst.Multiply(1, 3);

        Assert.AreEqual(3, res);
>>>>>>> dab3e7c76b152c5f6671bd86675b7f7d8dc1b450
    }
}
```

As result all appropriate test cases are created to "Composite Fixtures"

Explanation:

CompositeTestFixture attribute: 
* marks class as test fixture, 
* allow specify list of embedded test fixtures and 
* then pass instance of class as factory to all embedded fixtures;

EmbeddedTestFixture attribute: 
* excludes fixture from test to run (and from Tests Exploref). Since NUnit 2.5 TestFixture attribute is optional and any class that contains Test or TestCase (and conforms few conditions) is treated as TestFixture and tests are visible in Test Explorer.
* allows to specify list of nested test fixtures.

ObjectFactoryHolder class:
* gives access to factory object.

Few notes:
* Important: It is required for CompositeTestFixture to implement factory interface. Current implementation of Pk.NUnitAttribute uses CompositeTestFixture’s instance as factory instance and passes it to EmbeddedTestFixtures. If fixture doesn't implement compatible interface, tests will fail (this.Factory.Create() will throw AssertionException).
* As you can see in IDividerTests, ObjectFactoryHolder can be used as member of fixture, so inheritance still can be used.
* Any interface of class types can be used as factory. But common using generic interface allows to leverage covariance, so only one compatible interface should be implemented
* Example group test by interface. It's possible to implement separate fixtures for separate methods of the same interface, parent class, etc.

## Composite test cases ##

In some scenarios different function could behave in a same way. For example, Division and Mod operations should throw DivideByZeroException in case of attempt divide number by zero.

In this case you can leverage “Composite test case” feature.

First, you should define delegate that will call Division and Mod operations, create custom attribute that derived from CompositeTestCaseAttribute and pass to base constructor array of ValueTuple that consist of function that will be called and its name (names will be added to test-case name):
```csharp
public delegate int DivisionTests(IDivider o, int a, int b);

public class DivisionOpTestCaseAttribute : CompositeTestCaseAttribute
{
    public DivisionOpTestCaseAttribute(params object[] args)
        : base(args, Build<DivisionTests>(("Div", (o, a, b) => o.Divide(a, b)),
                                        ("Mod", (o, a, b) => o.Mod(a, b))))
    {
    }
}
```

Then you can use new attribute as following:

```csharp
[DivisionOpTestCase(100, 0)
[DivisionOpTestCase(0, 0, TestName = "Divide 0 by 0")]
public void DivisionInvalidOperation(DivisionTests divider, int val1, int val2)
{
    var tst = this._Factory.Factory.Create();

    Assert.Throws<DivideByZeroException>(() => { int res = divider(tst, val1, val2); });
}
```

As result, for each DivisionOpTestCase framework creates two separate test-cases with same arguments, except delegate and with name FunctionName(TestName)(NameSpecifiedInArray). For example for line

```csharp
[DivisionOpTestCase(100, 0")]
```
framework creates two separate tests: “DivisionInvalidOperation(100, 0)(Div)” and “DivisionInvalidOperation(100, 0)(Div)”

Notes:
* This feature is not intended to reduce number of lines of code. But it helps to avoid copy-paste mistakes. For example, avoid mistake like “After fixing bug developer added test-case for Divide() and forget add same case for Mod()”
* Build<> function is a static function of CompositeTestCaseAttribute class and used as sort-of shorthand to build array of tuples. You can create array with new keyword, but in this case you will need explicitly cast each lambda to appropriate delegate.
* First parameter of test function is delegate. Developer should use it to run corresponding method of test instance. Framework is responsible to pass appropriate instance of delegate from the array.
* Similar functionality can be leveraged by standard TestCaseSource feature of NUnit. However, current approach allows to define test-case data inline. 

## Decorative attributes ##
**HiddenTestFixture**
As it was mentioned, since NUnit 2.5 TestFixture attribute is optional and any class that contains Test or TestCase (and conforms few conditions) is treated as TestFixture and tests are visible in Test Explorer. If in some reason you want to exlude some fixture from test-run and hide it in test explorer - you can mark it by this attribute

**DecoratedTestCase**
Behaves in a same way as TestCase, but adds test-method name to the test-name. Intension to implement and use this attribute caused by strange behavior of some Test Explorers that use TestName as main group attribute. Dispite the fact that FullTestName (internal property) contains method name, some test explorers shows only TestName, so that tests grouped not by method, but by human-readable description that developer provided. See comments in example:

```csharp
[TestCase(0, 0, 0, TestName = "a=b=0")] //TestMethod name substituted by TestName. In some Test Explorers just "a=b=0" will be displayed and used to group tests
[DecoratedTestCase(0, 1, 0, TestName = "a=0, b>0")] //Test name visible in Test Explorer is combination of TestMethodName(TestName)
[TestCase(2, 6, 12)]
[DecoratedTestCase(-2, -6, 12)] //Without sprcified test name appearance shold be the same as usual TestCase
[TestCase(-2, 6, -12)]
public void Multiplication(int val1, int val2, int expResult)
{
    var tst = this.Factory.Create();

    int res = tst.Multiply(val1, val2);

    Assert.AreEqual(expResult, res);
}
```

