﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************

using System;
using System.Collections.Generic;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Builders;
using Pk.NUnitExtension.Internals;

namespace Pk.NUnitExtension.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class CompositeTestFixtureAttribute : NUnitAttribute, IFixtureBuilder
    {
        private readonly NUnitTestFixtureBuilder _Builder;
        private readonly Type[] _Fixtures;

        public CompositeTestFixtureAttribute(params Type[] fixture)
        {
            this._Fixtures = fixture;
            this._Builder = new NUnitTestFixtureBuilder();
        }

        public IEnumerable<TestSuite> BuildFrom(ITypeInfo type)
        {
            TestSuite parentFixture = _Builder.BuildFrom(type, EmptyPreFilter.Default);
            string commonPrefix = parentFixture.ClassName + ".";

            var fixtureParameters = new TestFixtureParameters();
            fixtureParameters.Properties.Set(Constants.FactoryPropName, Activator.CreateInstance(type.Type));
            //Add all embedded fixtures and it's tests
            AddEmbeddedFixtures(parentFixture, fixtureParameters, this._Fixtures, commonPrefix);
            yield return parentFixture;
        }

        private static readonly Type _FixtureProviderType = typeof(IEmbeddedFixtureProvider);

        private void AddEmbeddedFixtures(TestSuite parentFixture, TestFixtureParameters fixtureParameters,
                                        Type[] types, string commonPrefix)
        {
            foreach (var embeddedFixtureType in types)
            {
                TestSuite embeddedFixture = _Builder.BuildFrom(new TypeWrapper(embeddedFixtureType),
                                    EmptyPreFilter.Default,
                                    fixtureParameters);

                //Update names of tests, so they will be visible in test explorer as part of the main fixture
                foreach (Test test in embeddedFixture.Tests)
                {
                    UpdateTestName(test, commonPrefix);
                }
                parentFixture.Add(embeddedFixture);

                //Recursion. Assumint that nested hierarchy is reasonably limited
                AddEmbeddedFixtures(parentFixture, fixtureParameters,
                        GetNestedEmbeddedFixtures(embeddedFixtureType), commonPrefix);
            }
        }

        private Type[] GetNestedEmbeddedFixtures(Type type)
        {
            object[] attributes = type.GetCustomAttributes(false);
            foreach(object attribute in attributes)
            {
                if (attribute is IEmbeddedFixtureProvider provider)
                {
                    //Only one EmbeddedFixtureAttribute is supported
                    return provider.EmbeddedFixtures;
                }
            }
            return Array.Empty<Type>();
        }

        private void UpdateTestName(Test test, string prefix)
        {
            if (test.TestCaseCount > 1)
            {
                foreach (Test t in test.Tests)
                {
                    //Using recursion... Low probability of problems
                    UpdateTestName(t, prefix);
                }
            }
            else
            {
                test.FullName = prefix + test.Name;
            }
        }
    }
}
