﻿// ***********************************************************************
// Copyright (c) 2019 Pavlo Kruglov
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Builders;

namespace Pk.NUnitExtension.Attributes
{

    /// <summary>
    /// Base class for test case attributes that will use delegate
    /// as a first parameter in test case method
    /// <example>
    /// <code>
    /// <![CDATA[
    /// public delegate int MultiplicationAction(IMultiplicator o, int a, int b);
    ///
    /// public class CommutativeTestCaseAttribute : CompositeTestCaseAttribute
    /// {
    ///    public CommutativeTestCaseAttribute(params object[] args)
    ///        : base(args, Build<MultiplicationAction>(("Normal order", (o, a, b) => o.Multiply(a, b)),
    ///                                      ("Reversed order", (o, a, b) => o.Multiply(a, b))))
    ///    {
    ///    }
    /// }
    /// ]]>
    /// </code>
    /// </example>
    /// </summary>

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public abstract class CompositeTestCaseAttribute : NUnitAttribute, ITestBuilder
    {
        public string TestName { get; set; }

        private readonly object[] _Args;
        private readonly (string Name, object Value)[] _Cases;
        private readonly NUnitTestCaseBuilder _Builder;

        public CompositeTestCaseAttribute(object[] args,
                                        (string Name, object Value)[] cases)
        {
            this._Args = args;
            this._Cases = cases;
            this._Builder = new NUnitTestCaseBuilder();
        }

        public IEnumerable<TestMethod> BuildFrom(IMethodInfo method, Test suite)
        {
            foreach (var c in this._Cases)
            {
                var testData = new TestCaseData(Enumerable.Repeat((object)c.Value, 1)
                                                        .Concat(this._Args).ToArray());
                string name;
                if (String.IsNullOrEmpty(this.TestName))
                {
                    name = $"{method.Name}({String.Join(", ", this._Args)})({c.Name})";
                }
                else
                {
                    name = $"{method.Name}({this.TestName})({c.Name})";
                }
                testData.SetName(name);
                yield return _Builder.BuildTestMethod(method, suite, testData);
            }
        }

        protected static (string name, object value)[] Build<T>(params (string name, T value)[] cases)
            => cases.Select((o) => (o.name, (object)o.value)).ToArray();
    }
}
